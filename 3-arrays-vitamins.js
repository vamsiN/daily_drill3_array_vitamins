const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


// 1. Get all items that are available 
const availableItems = items.filter((function(each){
    return each.available
}))
//console.log(availableItems)


// 2. Get all items containing only Vitamin C.
const itemsWithVitaminC = items.filter(function(each){
    return each.contains.split(", ").includes("Vitamin C")
})
//console.log(itemsWithVitaminC)

// 3. Get all items containing Vitamin A.
const itemsWithVitaminA = items.filter(function(each){
    return each.contains.split(", ").includes("Vitamin A")
})
//console.log(itemsWithVitaminA)

//console.log(itemsWithVitaminA)

// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }    
//     and so on for all items and all Vitamins.

const vitaminItemsObject ={}

items.map(function(each){
    let vitaminsInItemArray = each.contains.split(", ")

    vitaminsInItemArray.forEach(function(eachVitamin){
        if(eachVitamin in vitaminItemsObject){
            vitaminItemsObject[eachVitamin].push(each.name)
        }else{
            vitaminItemsObject[eachVitamin]=[each.name]
            
        }
    })
})
console.log(vitaminItemsObject)


// 5. Sort items based on number of Vitamins they contain.
const sortedItems = items.sort(function(a,b){
    if(a.contains.split(", ").length > b.contains.split(", ").length){
        return 1
    }else if(a.contains.split(", ").length < b.contains.split(", ").length){
        return -1
    }else{
        return 0
    }
})
//console.log(sortedItems)



